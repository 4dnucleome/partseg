import sys

from PyQt5.QtWidgets import QDialog, QPushButton, QTextEdit, QHBoxLayout, QVBoxLayout, QLabel
import traceback
import partseg_utils.report_utils as report_utils
import sentry_sdk


class ErrorDialog(QDialog):
    def __init__(self, exception: Exception, description: str, additional_notes: str = ""):
        super().__init__()
        self.exception = exception
        self.additional_notes = additional_notes
        self.send_report_btn = QPushButton("Send information")
        self.cancel_btn = QPushButton("Cancel")
        self.error_description = QTextEdit()
        self.error_description.setText("".join(
            traceback.format_exception(type(exception), exception, exception.__traceback__)))
        self.error_description.append(str(exception))
        self.error_description.setReadOnly(True)
        self.additional_info = QTextEdit()

        self.cancel_btn.clicked.connect(self.reject)
        self.send_report_btn.clicked.connect(self.send_information)

        layout = QVBoxLayout()
        self.desc = QLabel(description)
        self.desc.setWordWrap(True)
        layout.addWidget(self.desc)
        layout.addWidget(self.error_description)
        layout.addWidget(QLabel("Additional information from user:"))
        layout.addWidget(self.additional_info)
        btn_layout = QHBoxLayout()
        btn_layout.addWidget(self.cancel_btn)
        btn_layout.addWidget(self.send_report_btn)
        layout.addLayout(btn_layout)
        self.setLayout(layout)

    def exec(self):
        if not report_utils.report_errors:
            sys.__excepthook__(type(self.exception), self.exception, self.exception.__traceback__)
            return False
        super().exec()

    def send_information(self):
        text = self.desc.text() + "\n"
        if len(self.additional_notes) > 0:
            text += "Additional notes: " + self.additional_notes + "\n"
        text += self.error_description.toPlainText() + "\n\n"
        if len(self.additional_info.toPlainText()) > 0:
            text += "User information:\n" + self.additional_info.toPlainText()
        sentry_sdk.capture_message(text)
        self.accept()
