import numpy as np

R2 = 1.41
R3 = 1.73

V = np.array([[-1, -1, -1], [0, -1, -1], [+1, -1, -1],
              [-1, 0, -1], [0, 0, -1], [+1, 0, -1],
              [-1, +1, -1], [0, +1, -1], [+1, +1, -1],

              [-1, -1, 0], [0, -1, 0], [+1, -1, 0],
              [-1, 0, 0],               [+1, 0, 0],
              [-1, +1, 0], [0, +1, 0], [+1, +1, 0],

              [-1, -1, +1], [0, -1, +1], [+1, -1, +1],
              [-1, 0, +1], [0, 0, +1], [+1, 0, +1],
              [-1, +1, +1], [0, +1, +1], [+1, +1, +1]])

D = np.array([R3, R2, R3,
              R2, 1, R2,
              R3, R2, R3,

              R2, 1, R2,
              1, 1,
              R2, 1, R2,

              R3, R2, R3,
              R2, 1, R2,
              R3, R2, R3])